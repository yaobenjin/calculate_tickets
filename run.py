import yaml,sys
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from collections import OrderedDict
import json
from databo import Ticket
import redis
import time
import logging

import asyncio

def load_config():
    configfile = sys.argv[1]
    if configfile:
        return yaml.load(open(configfile,'r'))
    return yaml.load(open("/etc/maxwin/calculate_tickets/config.yaml","r"))

config = load_config()

database = config['database']
ticketConfig=config['ticket']
SQLALCHEMY_DATABASE_URI = 'mssql+pymssql://{user}:{password}@{server}:{port}/{dbname}'.format(**database)
engine = create_engine(SQLALCHEMY_DATABASE_URI,echo=False)
DBSession = sessionmaker(bind=engine)
session = DBSession()

rinfo=config['redis']
rds = redis.Redis(**rinfo)
rds_key='calculate_tickets:lrs'
rds_key_result='calculate_tickets:result'


async def calculate_tickets():
    try:
        if rds.llen(rds_key)==0:
            return
        msg =''
        try:
            msg = rds.get(rds_key_result)
        except Exception as err:
            pass
        lgxno=rds.lrange(rds_key,0,0)
        lgxno=int(lgxno[0].decode())
        session.execute("""
                DELETE ts  FROM Tickets as ts
                inner join routestop as rs on(ts.fromstopid=rs.id or ts.tostopid=rs.id)
                inner join route as r on(r.id=rs.routeid)
                inner join logicroute as lr on(lr.id=r.logicrouteid)
                inner join stop as st on(st.id=rs.stopid)
                where lr.xno=%d
            """ % (lgxno))
        session.commit()
        rss = session.execute("""
            select rs.id,rs.ticketDistance,goback=dbo.GetGoBack(lr.redrouteid,lr.greenrouteid,r.id) from routestop as rs
            inner join route as r on(r.id=rs.routeid)
            inner join logicroute as lr on(lr.id=r.logicrouteid)
            inner join stop as st on(st.id=rs.stopid)
            where lr.xno=%d and st.StopType<>2
            order by goback,rs.orderno
        """%(lgxno))
        sql=''
        lastGoBack=-1
        t_str=time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))
        for rs in rss:
            if lastGoBack!=rs.goback:
                routeStopDistances = OrderedDict()
            for key,value in routeStopDistances.items():
                calculationdistance=value+rs.ticketDistance
                stopfullticket =calculationdistance / 1000 * float(ticketConfig['distance_base'])
                stophalfticket =stopfullticket / 2
                stopfullticket=int(round(stopfullticket))
                stophalfticket=int(round(stophalfticket))
                if stopfullticket<ticketConfig['full']:
                    stopfullticket=ticketConfig['full']
                if stophalfticket < ticketConfig['half']:
                    stophalfticket=ticketConfig['half']
                routeStopDistances[key]=calculationdistance
                sql+="""
                  INSERT INTO [Tickets] (type, time, vtype, price, currency, period, fromstopid, tostopid, insertdatetime, description)
                  VALUES (%d,0,1,%f ,'NTD', '','%s', '%s', '%s', '%s');
                    """ % (1,stopfullticket,key,rs.id,t_str,'calculate_tickets')
                sql +="""
                  INSERT INTO [Tickets] (type, time, vtype, price, currency, period, fromstopid, tostopid, insertdatetime, description)
                  VALUES (%d,0,1,%f ,'NTD', '','%s', '%s', '%s', '%s');
                    """ % (2,stophalfticket,key,rs.id,t_str,'calculate_tickets')
            routeStopDistances[rs.id]=0
            lastGoBack=rs.goback
        session.execute(sql)
        session.commit()
        session.close()
        rds.lpop(rds_key)

        #json.dumps({'message':'計算成功','xno': lgxno,'success': True})
        rds.set(rds_key_result,msg.decode()+";路線編號為["+str(lgxno)+"]計算成功")
    except Exception as e:
        try:
            session.rollback()
        except Exception as err:
            #str(err)
            rds.set(rds_key_result,msg.decode()+";路線編號為["+str(lgxno)+"]計算失敗 原因:"+str(err))
            logging.error(str(err))
            #return {'success': False, 'messages': str(err)}, 500, {"content-type": "chatset=utf8"}
        rds.set(rds_key_result,msg.decode()+";路線編號為["+str(lgxno)+"]計算失敗 原因:"+str(e))
        rds.lpop(rds_key)
        logging.error(str(e))

async def main():
    while True:
        await calculate_tickets()
        await asyncio.sleep(1)

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
    loop.close()