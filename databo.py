from sqlalchemy import Column, Integer, String,Float, DateTime
#from sqlalchemy.orm import relationship, backref, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class Ticket(Base):
    __tablename__ = 'Tickets'
    __table_args__ = {"implicit_returning": False}
    id = Column(String,primary_key=True)
    type = Column(Integer)
    time = Column(Integer)
    vtype = Column(Integer)
    price = Column(Float)
    currency = Column(String(50))
    period = Column(String(50))
    fromstopid = Column(String(50))
    tostopid = Column(String(50))
    insertdatetime = Column(DateTime)
    description =  Column(String(50))
